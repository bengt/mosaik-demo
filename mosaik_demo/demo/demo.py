import random
from os.path import abspath
from pathlib import Path

from householdsim.mosaik import HouseholdSim
from mosaik_csv import CSV
import mosaik  # Is provided by mosaik.Core_semver. Do not add to setup.py!
from mosaik.util import connect_randomly, connect_many_to_one
from mosaik_pypower.mosaik import PyPower
from mosaik_simconfig.simconfig.sim_config import SimConfig

sim_config = SimConfig()
sim_config.add_in_process(simulator=CSV)
sim_config.add_in_subprocess(command='mosaik-hdf5 %(addr)s',
                             simulator_name='DB')
# sim_config.add_in_subprocess(command='mosaik-householdsim %(addr)s',
#                              simulator_name='HouseholdSim')
sim_config.add_in_process(simulator=HouseholdSim)
# sim_config.add_in_subprocess(command='mosaik-pypower %(addr)s',
#                              simulator_name='PyPower')
sim_config.add_in_process(simulator=PyPower)
sim_config.add_in_subprocess(command='mosaik-web -s 0.0.0.0:8000 %(addr)s',
                             simulator_name='WebVis')

START = '2014-01-01 00:00:00'
ONE_MINUTE_IN_SECONDS = 60
ONE_HOUR_IN_SECONDS = 60 * ONE_MINUTE_IN_SECONDS
ONE_DAY_IN_SECONDS = 24 * ONE_HOUR_IN_SECONDS
ONE_MONTH_IN_SECONDS = 31 * ONE_DAY_IN_SECONDS
NEVER = float('inf')

ROOT_PATH = Path(abspath(__file__)).parent.parent.parent
PV_DATA = str(ROOT_PATH / 'data' / 'pv_10kw.csv')
PROFILE_FILE = str(ROOT_PATH / 'data' / 'profiles.data.gz')
GRID_NAME = 'demo_lv_grid'
GRID_FILE = str(ROOT_PATH / 'data' / (GRID_NAME + '.json'))


def run_forever():
    main(end=NEVER)


def run_for_one_month():
    main(end=ONE_MONTH_IN_SECONDS)


def run_for_one_day():
    main(end=ONE_DAY_IN_SECONDS)


def run_for_one_hour():
    main(end=ONE_HOUR_IN_SECONDS)


def run_for_one_minute():
    main(end=ONE_MINUTE_IN_SECONDS)


def main(end):
    random.seed(23)
    world = mosaik.scenario.World(sim_config)
    create_scenario(world)
    world.run(until=end)  # As fast as possible
    # world.run(until=END, rt_factor=1/60)  # Real-time 1min -> 1sec


def create_scenario(world):
    # Start simulators
    pypower = world.start('PyPower', step_size=15*60)
    house_hold_simulator = world.start('HouseholdSim')
    pv_simulator = world.start('CSV', sim_start=START, datafile=PV_DATA)

    # Instantiate models
    grid = pypower.Grid(gridfile=GRID_FILE).children
    houses = house_hold_simulator.ResidentialLoads(
        sim_start=START,
        profile_file=PROFILE_FILE,
        grid_name=GRID_NAME
    ).children
    pvs = pv_simulator.PV.create(20)

    # Connect entities
    connect_buildings_to_grid(world, houses, grid)
    connect_randomly(world, pvs, [e for e in grid if 'node' in e.eid], 'P')

    # Database
    db = world.start('DB', step_size=60, duration=ONE_MONTH_IN_SECONDS)
    hdf5 = db.Database(filename=str(ROOT_PATH / 'data' / 'demo.hdf5'))
    connect_many_to_one(world, houses, hdf5, 'P_out')
    connect_many_to_one(world, pvs, hdf5, 'P')

    nodes = [e for e in grid if e.type in ('RefBus, PQBus')]
    connect_many_to_one(world, nodes, hdf5, 'P', 'Q', 'Vl', 'Vm', 'Va')

    branches = [e for e in grid if e.type in ('Transformer', 'Branch')]
    connect_many_to_one(world, branches, hdf5,
                        'P_from', 'Q_from', 'P_to', 'P_from')

    # Web visualization
    web_visualisation_simulator = world.start(
        'WebVis',
        start_date=START,
        step_size=60
    )
    web_visualisation_simulator.set_config(
        ignore_types=[
            'Topology', 'ResidentialLoads', 'Grid', 'Database'
        ]
    )
    topology = web_visualisation_simulator.Topology()

    connect_many_to_one(world, nodes, topology, 'P', 'Vm')
    web_visualisation_simulator.set_etypes({
        'RefBus': {
            'cls': 'refbus',
            'attr': 'P',
            'unit': 'P [W]',
            'default': 0,
            'min': 0,
            'max': 30000,
        },
        'PQBus': {
            'cls': 'pqbus',
            'attr': 'Vm',
            'unit': 'U [V]',
            'default': 230,
            'min': 0.99 * 230,
            'max': 1.01 * 230,
        },
    })

    connect_many_to_one(world, houses, topology, 'P_out')
    web_visualisation_simulator.set_etypes({
        'House': {
            'cls': 'load',
            'attr': 'P_out',
            'unit': 'P [W]',
            'default': 0,
            'min': 0,
            'max': 3000,
        },
    })

    connect_many_to_one(world, pvs, topology, 'P')
    web_visualisation_simulator.set_etypes({
        'PV': {
            'cls': 'gen',
            'attr': 'P',
            'unit': 'P [W]',
            'default': 0,
            'min': -10000,
            'max': 0,
        },
    })


def connect_buildings_to_grid(world, houses, grid):
    buses = filter(lambda e: e.type == 'PQBus', grid)
    buses = {b.eid.split('-')[1]: b for b in buses}
    house_data = world.get_data(houses, 'node_id')
    for house in houses:
        node_id = house_data[house]['node_id']
        world.connect(house, buses[node_id], ('P_out', 'P'))


if __name__ == '__main__':
    run_for_one_month()
