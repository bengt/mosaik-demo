from mosaik_demo.demo.demo import main, run_for_one_hour, run_for_one_day, \
    run_for_one_minute, run_for_one_month


def test_demo_run_for_one_second():
    end = 1
    main(end=end)


def test_demo_run_for_one_minute():
    run_for_one_minute()


def test_demo_run_for_one_hour():
    run_for_one_hour()


def test_demo_run_for_one_day():
    run_for_one_day()


def _test_demo_run_for_one_month():
    # This test takes quite long and adds little value.
    # So skip it here.
    run_for_one_month()
